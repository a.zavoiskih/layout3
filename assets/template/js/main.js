let loader = document.querySelector('.lds-dual-ring');

loader.addEventListener('animationend', () => {
    let invisibleFeature = document.querySelector('.feature-invisible');
    invisibleFeature.classList.remove('feature-invisible');
    invisibleFeature.classList.add('feature-visible');
});

Element.prototype.slider = function ({
                                         slideClass = '.slide',
                                         nextButtonClass = '.arrow-next',
                                         previousButtonClass = '.arrow-prev'
                                     } = {}) {
    let sliderID = `#${this.id}`;
    let slides = [];
    let currentSlideIndex = 0;

    this.initSlider = function () {
        document.querySelectorAll(`${sliderID} ${slideClass}`).forEach((slide, index) => {
            slides[index] = slide;
            if (slide.classList.contains('active')) {
                currentSlideIndex = index;
            }
        });

        document.querySelector(`${sliderID} ${nextButtonClass}`).addEventListener('click', () => {
            this.goToNextSlide();
        });

        document.querySelector(`${sliderID} ${previousButtonClass}`).addEventListener('click', () => {
            this.goToPreviousSlide();
        });
    }

    this.goToNextSlide = function () {
        this.goToSlide(currentSlideIndex + 1);
    }

    this.goToPreviousSlide = function () {
        this.goToSlide(currentSlideIndex - 1);
    }

    this.goToSlide = function (index) {
        if (index > slides.length - 1) {
            index = 0;
        }
        if (index < 0) {
            index = slides.length - 1;
        }

        slides[currentSlideIndex].classList.remove('active');
        slides[index].classList.add('active');
        currentSlideIndex = index;
    }

    this.initSlider();
}

document.getElementById('sliderHowDoesItWork').slider();

document.getElementById('sliderSendGiftsAndOrderForGroups').slider();

// https://stackoverflow.com/questions/2712120/javascript-plugin-creation
