// Task 1
console.log('Task 1');
// typeof(9)
// // Предположение: number
// // Фактический: number
//
// typeof(1.2)
// // Предположение: number
// // Фактический: number
//
// typeof(NaN)
// // Предположение: number
// // Фактический: number
//
// typeof("Hello World")
// // Предположение: string
// // Фактический: string
//
// typeof(true)
// // Предположение: boolean
// // Фактический: boolean
//
// typeof(2 != 1)
// // Предположение: boolean
// // Фактический: boolean
//
//
// "сыр" + "ы"
// // Предположение: сыры
// // Фактический: сыры
//
// "сыр" - "ы"
// // Предположение: NaN
// // Фактический: NaN
//
// "2" + "4"
// // Предположение: 24
// // Фактический: 24
//
// "2" - "4"
// // Предположение: -2
// // Фактический: -2
//
// "Сэм" + 5
// // Предположение: Сэм5
// // Фактический:
//
// "Сэм" - 5
// // Предположение: NaN
// // Фактический: NaN
//
// 99 * "шары"
// // Предположение: NaN
// // Фактический: NaN

// Task 2
let [rectangleSideA, rectangleSideB] = [2, 3];
let perimeter = (rectangleSideA + rectangleSideB) * 2;
let square = rectangleSideA * rectangleSideB;

console.log('Task 2')
console.log(`${rectangleSideA} ${rectangleSideB}`);
console.log(perimeter);
console.log(square);
console.log(perimeter / square);

// Task 3
let degreeCelsius = 22.5;
let degreeFahrenheit = 66.2;

let CelsiusToFahrenheit = degree => (degree * 9 / 5) + 32;
let FahrenheitToCelsius = degree => (degree - 32) * 5 / 9;

console.log('Task 3')
console.log(`${degreeCelsius}°C соответствует ${CelsiusToFahrenheit(degreeCelsius)}°F`);
console.log(`${degreeFahrenheit}°F соответствует ${FahrenheitToCelsius(degreeFahrenheit)}°C`);

// Task 4
console.log('Task 4');
// let year = +prompt('Введите год');
// alert(year % 4 === 0 && year % 100 !== 0);

// Task 5
console.log('Task 5');
let [number1, number2] = [5, 5];
console.log([number1, number2, number1 + number2].some(number => number === 10));

// Task 6
console.log('Task 6');
// natural_number = +prompt('Введите натуральное число:');
//
// let output = '';
// for (let i = 1; i <= natural_number; i++) {
//     output += `${i} овечка...`;
// }
//
// console.log(output);

// Task 7
console.log('Task 7');

for (let i = 0; i < 15; i++) {
    console.log(`${i} ${i % 2 === 0 ? 'чётное' : 'нечётное'}`);
}

// Task 8
console.log('Task 8');

for (let i = 1; i <= '############'.length; i++) {
    console.log(i % 2 === 1 ? '*'.repeat(i) : '#'.repeat(i));
}

// Task 9
console.log('Task 9');
let [x, y, z] = [0, -3, 1];
if (x > y && x > z) {
    if (y > z) {
        console.log(z + ", " + y + ", " + x);
    } else {
        console.log(y + ", " + z + ", " + x);
    }
} else if (y > x && y > z) {
    if (x > z) {
        console.log(z + ", " + x + ", " + y);
    } else {
        console.log(x + ", " + z + ", " + y);
    }
} else if (z > x && z > y) {
    if (x > y) {
        console.log(y + ", " + x + ", " + z);
    } else {
        console.log(x + ", " + y + ", " + z);
    }
}

// Task 10
console.log('Task 10');
let [a, b, c, d, e] = [2, -1, 0, -5, -4];

if (a >= b && a >= c && a >= d && a >= e) {
    console.log(a);
} else if (b >= a && b >= c && b >= d && b >= e) {
    console.log(b);
} else if (c >= a && c >= b && c >= d && c >= e) {
    console.log(c);
} else if (d >= a && d >= b && d >= c && d >= e) {
    console.log(d);
} else {
    console.log(e);
}
