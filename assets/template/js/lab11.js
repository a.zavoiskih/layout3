function absValue(number) {
    return number >= 0 ? number : -number;
}

// Task 1
console.log('Task 1');
for (let number of [-2, 100, 0]) {
    console.log(absValue(number));
}

// Task 2
console.log('Task 2');

function isPalindrome(word) {
    return word === [...word].reverse().join('');
}

for (let word of ['довод', 'кружка']) {
    console.log(isPalindrome(word));
}

// Task 3
console.log('Task 3');

function matrixAddition(matrix1, matrix2) {
    let output = ''
    if (matrix1.length === matrix2.length && matrix1[0].length === matrix2[0].length) {
        for (let i = 0; i < matrix1.length; i++) {
            for (let j = 0; j < matrix1[0].length; j++) {
                output += `${matrix1[i][j] + matrix2[i][j]}\t`
            }
            output += '\n'
        }
    } else {
        output = 'Операция невыполнима';
    }
    console.log(output);
}

matrixAddition([[1, 2], [3, 4]], [[9, 8], [7, 6]]);
matrixAddition([[1, 2]], [[9, 8], [7, 6]]);

// Task 4
console.log('Task 4');
let student = {
    group: 201,
    last_name: 'Иванов',
    first_name: 'Иван'
};
console.log(`Список свойств: ${Object.keys(student).join(', ')}`);
console.log(`Студент ${student.first_name} ${student.last_name} учится в ${student.group} группе`);
