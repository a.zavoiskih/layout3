# АНАЛИЗ СОВРЕМЕННЫХ САЙТОВ
## ЛАБОРАТОРНАЯ РАБОТА №10 по дисциплине «Веб-технологии»
**Выполнил**: Студент группы 211-321 Завойских А.С.
### Анализ сайтов и свойств
#### Подборка сайтов
| №   | Название сайта     | Cсылка                       | Краткое описание                                                                 |
|-----|--------------------|------------------------------|----------------------------------------------------------------------------------|
| 1.  | Williams Et Oliver | https://williams-oliver.ru/  | Интернет-магазин сети салонов кухонных товаров                                   |
| 2.  | EGLO               | https://eglo-msk.ru/         | b2b и b2c интернет-магазин международного производителя интерьерных светильников |
| 3.  | MebelVia           | https://eglo-msk.ru/         | крупнейший интернет-магазин мебели                                               |
| 4.  | BoConcept          | https://boconceptonline.ru/  | Брендовый интернет-магазин мебели                                                |
| 5.  | Cups от mail.ru    | https://cups.online/         | платформа чемпионатов it-специалистов                                            |
| 6.  | Аудиомания         | https://www.audiomania.ru/   | Мобильная версия крупнейшего интернет-магазина аудио и видео техники             |
| 7.  | ТРИО               | https://www.trio.ru/         | интернет-магазин сети салонов элитной мебели                                     |
| 8.  | ЦРТ-Инновации      | https://cloud.speechpro.com/ | сервис речевых технологий                                                        |
| 9.  | Дом Фарфора        | https://www.domfarfora.ru/   | Интернет-магазин премиального фарфора                                            |
| 10. | DDX Fitness        | https://ddxfitness.ru/       | сайт сети фитнесс-клубов                                                         |
#### Свойства
| №   | Синтаксис (правила), значения, прочая информация                                                                                                                                                                                   | Назначение                                                                                                                                                                                                                                                                                                                                                                                                              | Где встретилось (ссылка на страницу сайта)                 |
|-----|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------|
| 1.  | background-blend-mode: [normal, multiply, screen, overlay, darken, lighten, color-dodge, saturation, color, luminosity]                                                                                                            | The background-blend-mode CSS property sets how an element's background images should blend with each other and with the element's background color.                                                                                                                                                                                                                                                                    | https://www.ortizleon.es/                                  |
| 2.  | clip-path: [clip-source, basic-shape, margin-box, border-box, padding-box, content-box, fill-box, stroke-box, view-box, none, initial, inherit]. Лучше задавать свойство clip-path на этом сайте: https://bennettfeely.com/clippy/ | The clip-path CSS property creates a clipping region that sets what part of an element should be shown. Parts that are inside the region are shown, while those outside are hidden.                                                                                                                                                                                                                                     | https://stripe.com/                                        |
| 3.  | writing-mode: [horizontal-tb, vertical-rl, vertical-lr]                                                                                                                                                                            | The writing-mode property specifies whether lines of text are laid out horizontally or vertically.                                                                                                                                                                                                                                                                                                                      | https://www.youtube.com/watch?v=9GDUBrxwVUo                |
| 4.  | caret-color: [auto, color, initial, inherit]                                                                                                                                                                                       | The caret-color property specifies the color of the cursor (caret) in inputs, textareas, or any element that is editable.                                                                                                                                                                                                                                                                                               | https://www.youtube.com/watch?v=9GDUBrxwVUo                |
| 5.  | aspect-ratio: [auto, <ratio>]                                                                                                                                                                                                      | The aspect-ratio  CSS property sets a preferred aspect ratio for the box, which will be used in the calculation of auto sizes and some other layout functions.                                                                                                                                                                                                                                                          | https://youtu.be/Qhaz36TZG5Y                               |
| 6.  | clamp(MIN, VAL, MAX)                                                                                                                                                                                                               | The clamp() CSS function clamps a value between an upper and lower bound. clamp() enables selecting a middle value within a range of values between a defined minimum and maximum. It takes three parameters: a minimum value, a preferred value, and a maximum allowed value. The clamp() function can be used anywhere a <length>, <frequency>, <angle>, <time>, <percentage>, <number>, or <integer> is allowed.     | https://youtu.be/Qhaz36TZG5Y; https://youtu.be/JSURzPQnkl0 |
| 7.  | scroll-padding: [<length-percentage>, auto]                                                                                                                                                                                        | The scroll-padding-* properties define offsets for the optimal viewing region of the scrollport: the region used as the target region for placing things in view of the user. This allows the author to exclude regions of the scrollport that are obscured by other content (such as fixed-positioned toolbars or sidebars), or to put more breathing room between a targeted element and the edges of the scrollport. | https://youtu.be/JSURzPQnkl0                               |
| 8.  | pointer-events: [auto, none]                                                                                                                                                                                                       | Get rid of click ability of the button.                                                                                                                                                                                                                                                                                                                                                                                 | https://youtu.be/CxC925yUxSI                               |
| 9.  | scroll-behavior: [auto, smooth, initial, inherit]                                                                                                                                                                                  | The scroll-behavior property specifies whether to smoothly animate the scroll position, instead of a straight jump, when the user clicks on a link within a scrollable box.                                                                                                                                                                                                                                             | https://youtu.be/CxC925yUxSI                               |
| 10. | filter: [none, blur(), brightness(), contrast(), drop-shadow(), grayscale(), hue-rotate(), invert(), opacity(), saturate(), sepia(), url()]                                                                                        | The filter property defines visual effects (like blur and saturation) to an element (often <img>). The video in the links column uses filter to quickly create dark theme via couple lines of css.                                                                                                                                                                                                                      | https://youtu.be/CxC925yUxSI                               |
### Структура сайта
Характеристика: использована методология именования классов БЭМ, присутствуют html5-теги, местами диватоз, излишни некоторые обёртки div; в их html, css можно разобраться

[Ссылка на сайт](https://williams-oliver.ru/)
![Скриншот страницы](assets/template/image/williams-oliver.ru_.png)
#### Абсолютно споцизионированная шапка
1. Верхняя часть.header__top
   1. Поиск по каталогу (хотя визуально он находится в нижней части шапки).header__callbacks
      1. Поле ввода.header__catalog-box
      2. Иконка поиска.header__search
   2. Маленькое меню.header__small-menu
      1. О компании.header__small-menu-link
      2. Наши магазины.header__small-menu-link
      3. Доставка и оплата.header__small-menu-link
   3. Логотип.header__logo
   4. Телелефон (+7 495 644 34 48).header__phone
   5. Меню с иконками (которые сделаны через background-image).header-icon-menu
      1. Вход.header-icon-menu__text
      2. Связаться с нами.header-icon-menu__text
      3. Корзина.header-icon-menu__text
2. Нижняя часть.header__bottom
   1. Нижнее меню.header__bottom-menu
      1. Бренды.header__bottom-menu-link
      2. Новый год.header__bottom-menu-link
      3. Рождественский календарь.header__bottom-menu-link
      4. Посуда для приготовления.header__bottom-menu-link
      5. Сервизы и сервировка стола.header__bottom-menu-link
      6. Всё для бара.header__bottom-menu-link
      7. Декор интерьера.header__bottom-menu-link
   2. Выбор города (иконка которого сделана через background).header__location
#### Фиксированная шапка
1. Поиск по каталогу (хотя визуально он находится в нижней части шапки).header__callbacks
   1. Поле ввода.header__catalog-box
   2. Иконка поиска.header__search
2. Логотип.header__logo
3. Телелефон (+7 495 644 34 48).header__phone
4. Меню с иконками (которые сделаны через background-image).header-icon-menu
    1. Вход.header-icon-menu__text
    2. Связаться с нами.header-icon-menu__text
    3. Корзина.header-icon-menu__text
### Подвал
1. Логотип.footer__logo
2. Колонка (3 штуки).footer__columns-item
   1. Заголовок колонки.footer__columns-title
   2. Список колонки.footer__list
      1. Ссылки.footer__list-link
3. Отличающаяся по содержанию колонка.footer__columns-item
   1. Список колонки.footer__list
      1. Номер телефона.footer__list-link
      2. Номер телефона.footer__list-link
      3. Email.footer__list-link
   2. Ссылки на социальные сети.footer__social
      1. Facebook.footer__social-link
      2. VK.footer__social-link
      3. YouTube.footer__social-link
      4. Instagram.footer__social-link
4. Нижняя часть.footer__bottom
   1. Copyright.footer__copy
   2. Создатели сайта.footer__made
      1. Создание сайта.footer__made-item
         1. Студия 15.footer__made-link
      2. Дизайн сайта.footer__made-item
         1. ДзенДизайн.footer__made-link
### Основная часть
1. Slick slider (высотой 100vh).top-slider
   1. Pagination.slick-dots
   2. Стрелки влево и вправо.top-slider__arrow slick-arrow
   3. Слайд (высотой 100vh, относительно спозиционированный).top-slider__item
      1. Картинка (height: 100%; width: 100%; object-fit: cover; object-position: center; opacity: .7;).top-slider__img
      2. Блок с текстом.top-slider__box
         1. Заголовок слайда.top-slider__title
         2. Подзаголовок слайда.top-slider__text
2. Секция "Акции"
   1. Верхняя часть.section__top
      1. Заголовок секции.section__title
      2. Кнопка.section__top-button
   2. Карточка акции (3 штуки).grid-list-item
      1. Картинка.grid-list-item__img
      2. Блок с текстом.grid-list-item__content
         1. Заголовок акции.grid-list-item__title
         2. Подзаголовок акции.grid-list-item__subtitle
3. Баннеры.grid-banners
   1. Баннер.banner
      1. Картинка.banner__image
      2. Абсолютно споцизионированная панель.banner__panel
         1. Заголовок панели.panel__title
4. Баннер о компании.index-page__banner-about
   1. Картинка.banner__image
   2. Абсолютно споцизионированная панель.banner__panel
       1. Заголовок панели.panel__title
       2. Описание.panel__text
       3. Кнопка.panel__button
### Реализация блока "баннер о компании"
1. git clone https://gitlab.com/a.zavoiskih/layout3.git
2. Пишем в консоли npm run init
3. Создадим файл lab10.html
4. В него скопируем содержимое lab9.html
5. Изменим title в head на следующий:
```<title>Lab 10</title>```
6. Удалим содержимое body.
7. Подключим шрифты перед </head>.
```
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Prata&display=swap" rel="stylesheet">
```
8. Подключим стили перед </head>.
```<link rel="stylesheet" href="assets/template/style/css/style.min.css">```
9. Сохраним нужную нам картинку с сайта https://williams-oliver.ru/ как lab10.jpeg в каталог assets/template/image/
10. Вставляем в body следующий html:
```
<div class="container">
    <div class="index-page__banner-about">
        <img class="banner__image" src="assets/template/image/lab10.jpeg" alt="Williams Et Oliver">
        <div class="banner__panel">
            <h1 class="panel__title">Williams Et Oliver</h1>
            <p class="panel__text">Сеть магазинов правильных вещей для кухни, где собрана разнообразная, оригинальная и
                удобная кухонная посуда для ценителей кулинарного искусства.</p>
            <a class="panel__button">Подробнее</a>
        </div>
    </div>
</div>
```
11. Вставляем в файл main.scss следующее:
```
.index-page__banner-about {
  height: 710px;
  position: relative;
}

.banner__image {
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: left;
}

.banner__panel {
  background: hsla(0,0%,96.9%,.5);
  backdrop-filter: blur(20px);
  position: absolute;
  bottom: 0;
  left: 0;
  width: 480px;
  padding: 64px;
  text-align: center;
}

.panel__title {
  font-family: 'Prata', serif;
  font-size: 36px;
  line-height: 48px;
  letter-spacing: .04em;
  font-weight: 400;
}

.panel__text {
  margin-top: 23px;
  font-size: 18px;
  line-height: 26px;
}

.panel__button {
  display: inline-block;
  margin-top: 30px;
  border-radius: 20px;
  padding: 9px 30px;
  font-size: 14px;
  line-height: 20px;
  transition: color .25s ease,background-color .25s ease, border-color .25s ease;
  border: 1px solid #111;
  color: #111;
  cursor: pointer;

  &:hover {
    color: #111;
    border-color: #b8bd53;
  }
}
```
12. Открываем lab10.html в google chrome. 
13. Сверяем оригинал и пародию.
14. Радуемся их идентичности.
![Реализация блока "баннер о компании"](assets/template/image/localhost_3000_lab10.html.png)
### Ресурсы
| №   | Ссылка                                                                                                                                                            | Название                      | Тип (лучше описание)                                                                                                                                                                                                                                                                                                                                                                                                                                     | Как вы его используете       |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|
| 1   | https://www.youtube.com/c/Fireship                                                                                                                                | Fireship                      | code tutorials to help you build & ship your app faster. Subscribe for new videos every week covering intermediate to advanced lessons about JavaScript, Flutter, Firebase, and modern app development.                                                                                                                                                                                                                                                  | Смотрю интересные мне видео  |
| 2   | https://www.youtube.com/c/HowdyhoNet                                                                                                                              | Хауди Хо™ - Просто о мире IT! | здесь ты найдешь уроки по программированию, выпуски про хакинг, подборки, топы, гайды и всё такое из типичной жизни айтишника.При этом я стараюсь сделать это так, чтобы вообще любой человек понял о чём я говорю и чему учу. Ведь программирование и мир IT технологий - это так просто и интересно!                                                                                                                                                   | Колокольчик на все видео     |
| 3   | https://www.youtube.com/c/WebDevSimplified                                                                                                                        | Web Dev Simplified            | Web Dev Simplified is all about teaching web development skills and techniques in an efficient and practical manner. If you are just getting started in web development Web Dev Simplified has all the tools you need to learn the newest and most popular technologies to convert you from a no stack to full stack developer. Web Dev Simplified also deep dives into advanced topics using the latest best practices for you seasoned web developers. | Смотрю инетерсные мне видео  |
| 4   | https://www.youtube.com/c/Freecodecamp                                                                                                                            | freeCodeCamp.org              | Learn to code for free.                                                                                                                                                                                                                                                                                                                                                                                                                                  | Смотрю инетерсные мне видео  |
| 5   | https://www.youtube.com/c/pepelsbey                                                                                                                               | Vadim Makeev                  | Пепелсбей сам по себе, Google Developer Expert, руководитель «Веб-стандартов», организатор pitercss_meetup, автор движка Shower, хост подкастов LP и The F-Word.                                                                                                                                                                                                                                                                                         | Смотрю инетерсные мне видео  |
| 6   | [https://www.youtube.com/c/АкадемияЯндекса](https://www.youtube.com/c/%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D1%8F%D0%AF%D0%BD%D0%B4%D0%B5%D0%BA%D1%81%D0%B0) | Академия Яндекса              | Лекции из курсов различных школ Яндекса, записи мастер-классов, семинаров и докладов на мероприятиях — для специалистов IT-отрасли, студентов технических вузов и даже школьников.                                                                                                                                                                                                                                                                       | Колокольчик на все видео     |
| 7   | https://www.youtube.com/channel/UCmTxYyAdA3nkVuo-MFnm1ww                                                                                                          | Роман Довгий                  | Лучший наставник нашего потока                                                                                                                                                                                                                                                                                                                                                                                                                           | Колокольчик на все видео     |
| 8   | https://www.youtube.com/c/ITKAMASUTRA                                                                                                                             | IT-KAMASUTRA                  | Канал про программирование на JavaScript. Секреты и инсайды, как стать программистом абсолютно любому человеку.                                                                                                                                                                                                                                                                                                                                          | Смотрю интересные мне видео  |
| 9   | https://www.youtube.com/channel/UCTgx8cZRD5Jz2_zGaT27S3w                                                                                                          | Denis Gorelov                 | Канал DWS TV посвящен начинающим разработчикам, которым интересна сфера WEB технологий и хотят немного к ней приобщится. Вы найдете на моем канале как управлять сайтом на CMS 1С-Битрикс, разберем примеры как делать сайты с нуля, верстать различные элементы, освоим некоторые языки как CSS / CSS3, JavaScript, Flexbox, CSS Grid и другое из области WEB программирования.                                                                         | Смотрю интересные мне видео  |
| 10  | https://www.youtube.com/channel/UCaO6VoaYJv4kS-TQO_M-N_g                                                                                                          | Clément Mihailescu            | I'm an Ex-Google Software Engineer, an Ex-Facebook Software Engineer, and the CEO and co-founder of AlgoExpert (algoexpert.io), a website that helps Software Engineers prepare for coding interviews.                                                                                                                                                                                                                                                   | Смротю интрересные мне видео |