const gulp = require('gulp');
const spritesmith = require('gulp.spritesmith');
const svgSprite = require('gulp-svg-sprite');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const webp = require('gulp-webp');

let path = {
    'sprite': 'assets/template/image/sprite/*',
    'image': 'assets/template/image/',
    'css': 'assets/template/style/css/',
    'scss': 'assets/template/style/scss/*',
    'html': '*.html',
    'js': 'assets/template/js/*.js',
    'template': 'assets/template/'
};

function createSpritePNG() {
    let spriteData = gulp.src(path.sprite + '.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
        imgPath: '../../image/sprite.png',
        cssOpts: {
            cssSelector: function(sprite) {
                return '.icon-' + sprite.name.replace(/_/g, '-');
            }
        }
    }));

    spriteData.img.pipe(gulp.dest(path.image));
    return spriteData.css.pipe(gulp.dest(path.css));
}

function createSpriteSVG() {
    return gulp.src(path.sprite + '.svg')
        .pipe(svgSprite({
            mode: {
                symbol: {
                    dest: 'image',
                    sprite: 'sprite.svg'
                }
            },
            shape: {
                id: {
                    generator: function(fileName) {
                        spriteID = fileName.slice(0, -4);
                        camelCasedSpriteID = spriteID.replace(/_([a-z])/g, g => g[1].toUpperCase());
                        return camelCasedSpriteID;
                    }
                }
            }
        }))
        .pipe(gulp.dest(path.template));
}

function style() {
    return gulp.src(path.scss)
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(autoprefixer([
            'Android >= 2.2',
            'Chrome >= 3.5',
            'Firefox >= 3.5',
            'Explorer >= 8',
            'iOS >= 4.3',
            'Opera >= 10.1',
            'Safari >= 3',
            'IE >= 9'
        ]))
        .pipe(concatCss('style.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(path.css))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });

    gulp.watch(path.sprite + '.png', createSpritePNG);
    gulp.watch(path.sprite + '.svg', createSpriteSVG);
    gulp.watch(path.scss, style);
    gulp.watch([path.html, path.js]).on('change', browserSync.reload);
}

function produceWebPImages() {
    return gulp.src(path.image + '**/*.+(jpeg|jpg|png)')
        .pipe(webp())
        .pipe(gulp.dest(path.image + 'webp/'))
}

exports.default = watch;
exports.webp = produceWebPImages;
